package com.example.quicsolvpremiumapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorSet;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Context mContext;
    String edittag;
    private boolean isInRange = false;
    private boolean isSearchEnabled = false;
    Handler handler;
    private static final long SPLASH_TIME_OUT = 3000;
    private static final int REQUEST_PERMISSION = 42;
    TextView threats;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;

    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    ConstraintLayout theftProtectionButton, identityProtectionButton;

    boolean isScanStarted;
    boolean clickedButton = false;
    private static final String TAG = "HomeFragment";

    private static final String CHANNEL_ID = "QuicSolv";
    private static final String CHANNEL_ID1 = "QuicSolv1";
    private static final String CHANNEL_ID2 = "QuicSolv2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
     super.onCreate(savedInstanceState);
     setContentView(R.layout.activity_main);
     bindview();
     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
      NotificationChannel channel = new NotificationChannel(CHANNEL_ID,CHANNEL_ID1,NotificationManager.IMPORTANCE_HIGH);
      channel.setDescription(CHANNEL_ID2);
      NotificationManager manager = getSystemService(NotificationManager.class);
      manager.createNotificationChannel(channel);
     }
     checkForPermissions();
    }

    private void bindview() {
     mContext = this;
     theftProtectionButton = findViewById(R.id.theft_protection);
     identityProtectionButton = findViewById(R.id.identity_protection);
     threats = findViewById(R.id.threats);
     toolbar = findViewById(R.id.toolbar);
     drawerLayout = findViewById(R.id.drawerLayout);
     navigationView = findViewById(R.id.navigationView);
     setSupportActionBar(toolbar);
     if (getSupportActionBar() != null) {
      getSupportActionBar().setTitle("QuicSolv Premium App");
      toolbar.setTitle("QuicSolv Premium App");
     }
     actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
     actionBarDrawerToggle.syncState();
     drawerLayout.addDrawerListener(actionBarDrawerToggle);
     theftProtectionButton.setOnClickListener(this);
     identityProtectionButton.setOnClickListener(this);
    }

    void checkForPermissions() {
     if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
      searchBeacon();
     } else {
      ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
 //     searchBeacon();
     }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void foundDevice() {
     AnimatorSet animatorSet = new AnimatorSet();
     animatorSet.setDuration(400);
     animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
     ArrayList<Animator> animatorList = new ArrayList<Animator>();
     animatorSet.playTogether(animatorList);
     animatorSet.start();

     //  Scan Bluetooth here
     Timer timer = new Timer();
     if (timer != null) {
      timer.cancel();
      mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
      mBluetoothAdapter.enable();
      if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
       final BluetoothLeScanner bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
       timer = new Timer();
       timer.scheduleAtFixedRate(new TimerTask() {
        @Override
        public void run() {
        mBluetoothAdapter.stopLeScan(mBLeScanCallback);
         if (!isInRange) {

         }
         isInRange = false;
         mBluetoothAdapter.startLeScan(mBLeScanCallback);
         }
        }, 1000, 3000);
       }
      }
     }

    private BluetoothAdapter.LeScanCallback mBLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            int startByte = 2;
            boolean patternFound = false;
            while (startByte <= 5) {
                if (((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15) { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }

            if (patternFound) {
                //Convert to hex String
                byte[] uuidBytes = new byte[16];
                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = bytesToHex(uuidBytes);
                String uidString = bytesToHex(scanRecord);
                String wholeString = bytesToHex(scanRecord).substring(15, 17);
                String transmissionPower = bytesToHex(scanRecord).substring(64, 66);
                Log.i(TAG, "onLeScanWhole: " + transmissionPower);
                String minor1 = bytesToHex(scanRecord).substring(47, 50);
                Log.i(TAG, "onLeScanWhole: " + minor1);

                String check = uidString.substring(84, 86);

                //Here is your UUID
                String uuid = hexString.substring(0, 8) + "-" +
                        hexString.substring(8, 12) + "-" +
                        hexString.substring(12, 16) + "-" +
                        hexString.substring(16, 20) + "-" +
                        hexString.substring(20, 32);
                Log.i(TAG, "onLeScan: " + check);
                int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);
                final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);
                Log.i(TAG, "onLeScan: " + major);
                Log.i(TAG, "onLeScan: " + minor);

                if (check.equals("01")) {
                    if (wholeString.equals("21") && uuid.contains("9804660A-1515-5876-5864-8F")) {
                        if (transmissionPower.equals("F0")) {
                            if (isSearchEnabled) {
                                isInRange = true;
                                createNotification("QuicSolv Premium App", "Tag with magnet found");
                                threats.setText("Tag with magnet found");

                            } else {
                                Toast.makeText(mContext, "Tag not found", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            //  Toast.makeText(mContext, "21 not found", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        // Toast.makeText(mContext, "01 not found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    createNotification("QuicSolv Premium App", "Tag without magnet found");
                    threats.setText("Tag without magnet found");
                }


            }
        }
    };

    static final char[] hexArray = "0123456789ABCDEF".toCharArray();

    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.theft_protection:

                break;

            case R.id.identity_protection:
                break;
        }
    }

    public void searchBeacon() {
        if (!isSearchEnabled) {
         LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
         final LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
         if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
         }

         BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
         if (!mBluetoothAdapter.isEnabled()) {
          mBluetoothAdapter.enable();
         }

         clickedButton = true;
         isSearchEnabled = true;
         isScanStarted = false;
         clickedButton = true;
         isScanStarted = true;
         foundDevice();
         if (!isScanStarted) {
          Prefs.setSharedPreferenceString(mContext, Prefs.PREF_SCANNED_RECORD_DATA, "");
          mBluetoothAdapter.enable();
          if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
           final BluetoothLeScanner bluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();
           if (bluetoothLeScanner != null) {
            mBluetoothAdapter.stopLeScan(mBLeScanCallback);
           }
          }
         }
        }
            }


    private void createNotification(String title, String msg) {
        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);

            NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(this);
            mNotificationManager.notify(1,mBuilder.build());
        }else{
            Notification notification = new Notification.Builder(this)
                    .setContentTitle(title)
                    .setContentText(msg)
                    .setSmallIcon(R.drawable.painting)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .build();
            NotificationManager notificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(111, notification);
        }

    }
}
